#ifndef MAZE_GRAPH_H
#define MAZE_GRAPH_H
#include "List.h"

#define INF -1

#define BLACK -1
#define WHITE 0
#define GRAY 1

typedef struct Graph{
    int size; //edges
    int order; //vertices
    ListNode **adj; //adjacency list;
    int* color;
    int* dist;
    int* parent;
}Graph;

Graph* newGraph(int order);
void delGraph(Graph* G);

void addEdge(Graph* G, int u, int v);

#endif //MAZE_GRAPH_H
