#include "Graph.h"
#include "List.h"
#include <stdlib.h>
#include <string.h> //memset;


Graph* newGraph(int order){ //graph data structure;
    Graph* g = malloc(sizeof(Graph)); //allocate container.
    g -> size = 0; //edges
    g -> order = order; //vertices
    g->adj = (ListNode **) malloc(sizeof(ListNode *) * order); //adjacency list;
    for(int i = 0; i < order; i++)
    {
        g->adj[i] = newNode(); //init each node.
    }

    g->color = memset(malloc(sizeof(int) * order), WHITE, order * sizeof(int)); //set node color to white;
    g->dist = memset(malloc(sizeof(int) * order), INF, order * sizeof(int)); //set node distances to infinite;
    g->parent = memset(malloc(sizeof(int) * order), 0x0, order * sizeof(int)); //set parents to null.
}

void delGraph(Graph* G){ //free graph.
    for (int i = 0; i < G->order; i++) {
        delList(G->adj[i]);
    }
    free(G->adj);
    free(G->color);
    free(G->dist);
    free(G->parent);
    free(G);

}

void addEdge(Graph* G, int u, int v){ //create an edge.
    append(G->adj[u], newNode(v));
    append(G->adj[v], newNode(u));
}
