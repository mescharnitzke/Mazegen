//
// Created by Brennan on 8/18/2017.
//

#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include <memory.h>
#include "Bitmap.h"



bitmap *newMap(uint32_t width, uint32_t height) {
    bitmap *map = malloc(sizeof(bitmap));
    map->fileHeader = (FHEADER) {.magic = MAGIC};
    map->infoHeader = (IHEADER){.hsize = 40,
            .colorPlanes = 1, .pixelSize = PIXSIZE,
            .compMethod = BI_RGB, .imgSize = BI_RGB,
            .width = width, .height = height};
    map->fileHeader.imgOff = sizeof(FHEADER) + sizeof(IHEADER)  +  + 1024;
    map->infoHeader.imgSize = (rowSize(map)* height);
    map->fileHeader.fsize = map->fileHeader.imgOff + map->infoHeader.imgSize;
    map->image = malloc(sizeof(*map->image) * height);
    if (map->image)
    {
        for (int i = 0; i < height; i++)
        {
            map->image[i] = malloc(rowSize(map));
        }
    }
    map->image[7][9] = 0xFF;
    map->image[2][2] = 0xFF;
    return map;

}

void freeMap(bitmap* map)
{
    for(int i = 0; i < map->infoHeader.height; i++)
    {
        free(map->image[i]);
    }
    free(map->image);
    free(map);
}

void setRow(bitmap *map, uint8_t *rowData, uint32_t rowNum) {

}

uint32_t rowSize(bitmap *map) {
    return floor((map->infoHeader.pixelSize * map->infoHeader.width + 31) / 32) * 4;
}

void setPixel(bitmap* map, uint32_t row, uint32_t col, uint8_t color){
    map->image[col][row] = color;
}


void toFile(char* name, bitmap* map)
{

    FILE* file = fopen(name, "w");
    fwrite(&map->fileHeader, 1 , sizeof(FHEADER), file);
    fwrite(&map->infoHeader, 1,  sizeof(IHEADER), file);
    uint32_t buffer[256] = { 0 };
    buffer[255] = 0xFFFFFFFF;
    fwrite(buffer, 4, 256, file);
    for(int i = 0; i < map->infoHeader.height; i++)
    {
        fwrite(map->image[i], 1, rowSize(map), file);
    }

    fclose(file);
}
