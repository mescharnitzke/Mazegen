#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Bitmap.h"
#include "Graph.h"

//converts a grid coordinate to a grid node id.
#define toID(x, y, h) ((y*h)+x)

//converts xy coordinates to the coordinate between them.
// (or at least thats what it should do, I was kinda in the middle of testing it.)
#define getWall(a, b) (abs((a*2)-1 - (b*2)-1))

//isn't really used; Should convert this to the graph, but eeeh..
typedef struct Cell{
    int cellColor;
    int nodeColor;
    struct Cell* parent;
}Cell;

//remove walls between connected nodes. Currently being used to test the graph.
void removeWalls(bitmap* map, Graph* G)
{
    for(int i = 0; i < G->order; i++) //for all nodes
    {
        ListNode* node = G->adj[i]->next; //get all connected nodes.
        if(node != NULL){ // if it's valid
            for(node; node -> next != NULL; node = node -> next) //<crash?>
            {
            }
        }
    }
}

//init the maze.
void initMaze(bitmap* map, uint32_t imageWidth, uint32_t imageHeight)
{
    //grid size.
    int gridWidth = imageWidth/2; int gridHeight = imageHeight/2;
    //init graph
    Graph *G = newGraph(gridHeight * gridWidth);

    //cycle through pixels
    for ( int i = 0; i < imageWidth; i++ )
    {
        for ( int j = 0; j < imageHeight; j++ )
        {
            //set the alternating ones to white.
            if ( i * j % 2 )
            {
                int gridx = i/2; int gridy = j/2; //convert pixel to graph coordinates.
                int gridID = (gridx * gridHeight) + gridy; //create an incrementing ID based on position.
                setPixel(map,i, j, WHITE_COLOR); //make it white

                if(gridy != gridHeight-1)// if not top row
                {
                    //printf("x: %d y: %d id: %d\n", gridx, gridy, toID(gridx, gridy, gridHeight));
                    addEdge(G, toID(gridx, gridy, gridHeight), toID(gridx, (gridy+1), gridHeight));
                    //printf("%d <--> %d\n", toID(gridx, gridy, gridHeight), toID(gridx, (gridy+1), gridHeight));

                }
                if(gridx != gridWidth-1) //if not right edge.
                {
                    addEdge(G, toID(gridx, gridy, gridHeight), toID((gridx+1), gridy, gridHeight));
                    //printf("%d <--> %d\n", toID(gridx, gridy, gridHeight), toID((gridx+1), (gridy), gridHeight));
                }

            }
            else setPixel(map,i, j, BLACK_COLOR);; //Add walls between nodes
        }

    }
    removeWalls(map, G);
}

int main() {
    int width = 11; //image size
    int height = 11; //image size

    Cell** cells = malloc(width*sizeof(Cell*)); //cell data structure becauuuuuse.... idk.
    for(int i = 0; i< width; i++)
    {//init
        cells[i] = malloc(height * sizeof(Cell));
        for(int j = 0; j<height; j++)
        {
            cells[i][j].parent = NULL;
            cells[i][j].cellColor == BLACK;
        }
    }

    //create map
    bitmap* map = newMap(width, height);
    //file name... for some reason.;
    char name[8] = "out.bmp";
    initMaze(map, width, height);


    //write memory to file.
    toFile("out.bmp", map);
    freeMap(map); //del memory;
}
