CC = gcc

OBJECTS = main.o Bitmap.o Graph.o List.o
BINARIES = mazegen mazegen.exe
.PHONY:
all: mazegen

mazegen : $(OBJECTS)
	$(CC) $(OBJECTS) -o mazegen -lm

Graph.o : Graph.c Graph.h List.h
	$(CC) -c -g Graph.c

List.o : List.c List.h
	$(CC) -c -g List.c

Bitmap.o : Bitmap.c Bitmap.h
	$(CC) -c -g Bitmap.c

main.o : main.c Graph.h
	$(CC) -c -g main.c

.PHONY:
clean :
	rm -f $(OBJECTS) $(BINARIES) $(wildcard *.bmp)
