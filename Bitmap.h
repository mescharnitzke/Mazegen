//
// Created by Brennan on 8/18/2017.
//

#ifndef MAZETHING_BITMAP_H
#define MAZETHING_BITMAP_H

#include <stdint.h>
#define MAGIC 0x4D42

#define PIXSIZE 8 // 1 byte per pixel, white (0xFF) or black(0x00).
#define CCOUNT 0; //color count; 0 to default to 2^n
#define BI_RGB 0

#define WHITE_COLOR 0xFF
#define BLACK_COLOR 0x00

#define FHEADER struct BITMAPFILEHEADER
#define IHEADER struct BITMAPINFOHEADER

#pragma pack(2)
struct BITMAPFILEHEADER
{
    uint16_t magic;// = BM;
    uint32_t fsize;//filesize;
    uint16_t res1;// = 0; //reserved
    uint16_t res2;// = 0; //reserved;
    uint32_t imgOff; //image offset
}__attribute__((packed));

struct BITMAPINFOHEADER
{
    uint32_t hsize;// = 40; //header size
    int32_t width; //width in pixels;
    int32_t height; //height in pixels;
    uint16_t colorPlanes;// = 1;
    uint16_t pixelSize;// = PIXSIZE; //bits per pixel (color depth)
    uint32_t compMethod;// = BI_RGB;
    uint32_t imgSize;// = BI_RGB;//dummy zero for RGB bitmaps.
    int32_t xres;// = 0; //horiz res (pixel per meter)
    int32_t yres;// = 0; //vert res (pixel per meter)
    uint32_t numColors;// = CCOUNT;
    uint32_t impColors;// = 0;
};

typedef struct bitmap
{
    FHEADER fileHeader;
    IHEADER infoHeader;
    uint8_t** image;

}bitmap;


bitmap* newMap(uint32_t width, uint32_t height);


void setRow(bitmap* map, uint8_t* rowData, uint32_t rowNum);

void setPixel(bitmap* map, uint32_t row, uint32_t col, uint8_t color);

uint32_t rowSize(bitmap* map);

void toFile(char* file, bitmap* map);

#endif //MAZETHING_BITMAP_H
