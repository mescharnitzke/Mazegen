#include "List.h"
#include <stdlib.h>

ListNode *newNode(int id){
    ListNode* node = malloc(sizeof(ListNode));
    node -> id = id;
    node -> next = (ListNode *) NULL;
    node -> prev = (ListNode *) NULL;
    return node;
}

void append(ListNode* head, ListNode* node){
    ListNode* index = head;
    while(index->next != NULL)
    {
        index = index -> next;
    }
    node -> prev = index;
    index -> next = node;
}

void prepend(ListNode** head, ListNode* node){
    node -> next = *head;
    (*head) -> prev = node;
    node -> prev = 0x0;
    *head = node;
}

void delNode(ListNode* node){
    free(node);
    node = NULL;
}

void delList(ListNode* head){
    ListNode* temp = head;
    while(temp -> next != NULL)
    {
        ListNode *next = temp -> next;
        delNode(temp);
        temp = next;
    }
}
